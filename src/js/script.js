document.addEventListener("DOMContentLoaded", function(event) {

    /*----------------------------------------------------------------------------------------------*/

    // Click ajouté sur le bouton de retour vers le haut de la page
    document.getElementById('retour').addEventListener('click', function(e) {
        scrollToTop();
    });

    // Variable pour le timer
    var timer;

    // Retourne la page en haut
    function scrollToTop() {
        // Vérifie si on est pas au début de la page
        if (document.body.scrollTop!=0 || document.documentElement.scrollTop!=0)
        {

            // On scroll de 50 pixels vers le haut
            window.scrollBy(0,-50);

            // On lance un setTimeout qui rappelle encore la fonction après le délai
            // C'est le setTimeout qui permet de faire l'effet de la page qui défile
            timer = setTimeout(scrollToTop, 10);
        }
        // Quand on est rendu au début de la page, on arrête le timer
        else {
            clearTimeout(timer);
        }
    }

    // Code repris de https://dzone.com/articles/back-to-top-pure-javascript

    /*----------------------------------------------------------------------------------------------*/

    // Ouvre la popup de connexion
    document.getElementById("connexion").addEventListener("click", function(e) {

        e.preventDefault();

        document.querySelector("#connexion-modale").classList.add("open");


    });

    // Ferme la popup de connexion
    document.getElementById("fermer-connexion-modale").addEventListener("click", function(e) {

        e.preventDefault();

        document.querySelector("#connexion-modale").classList.remove("open");


    });

    /*----------------------------------------------------------------------------------------------*/

    // Test de javascript
    console.log("Ça fonctionne");

});